## pi@raspberrypi:~ $ crontab -e
## @reboot sudo python3 quanta_weather_rs485_so/Bootloader.py
## @reboot sudo python3 quanta_weather_rs485_so/Bootloader_weather.py

import subprocess
import os
# import lamp_ctl
# import ipcam_ctl
# import watchdog_ctl
import weather_ctl
import Weather_service
import time
# import timerService
import energy_mesure
import _thread
# import MqttService as MqttService
import SendWeatherData
import bacup_data_service
import Constants
import Quanta_Env
import uuid
import check_connection
import LogService as LogService
log_service = LogService.LogService()

####### Set I/O on boot
weather_ctl.set_state(False)

def check_inet():
    #### Check Internet ####
    if check_connection.checkInternetHttplib():
        Constants.INET_CON = True
        return True
    else:
        Constants.INET_CON = False
        return False

try:
    os.system('pwd')
    os.chdir("quanta_weather_rs485_so")
    os.system('pwd')
except:
    pass

class main:
    def __init__(self):
        time.sleep(5)
        print(Constants.SOFTWARE_TITLE)
        time.sleep(1)
        weather_ctl.set_state(True)
        try:
            if Quanta_Env.STATION_INDEX == '0':
                if Quanta_Env.WEATHER_SERVICE == True:
                    _thread.start_new_thread(Weather_service.SensorService, ())
                    log_service.info('-Run Weather_service')
                    print('-Run Weather_service')

                    _thread.start_new_thread(SendWeatherData.SendWeatherData, ())
                    log_service.info('-### Run Send Weather DataS ervice')
                    print('\n-### Run SendDataService')

                    _thread.start_new_thread(bacup_data_service.BackupData, ())
                    log_service.info('-### Run Bacup data service')
                    print('\n-### Run Bacup data service')
            else:
                print('Disable weather service')
                log_service.info('Disable weatherservice')

            if Quanta_Env.ATM_SERVICE == True:
                import atm_sensor
                atm_sensor.mesure()
                Constants.ATM_EN = True
                log_service.info('-Run atm_sensor')
                print('-Run atm_sensor')
                _thread.start_new_thread(atm_sensor.mesure.read, ())
            else:
                print('Disable ATM service')
                log_service.info('Disable ATM service')

            if Quanta_Env.STATION_INDEX == '0':
                if Quanta_Env.ENERGY_SERVICE == True:
                    energy_mesure.mesure()
                    log_service.info('-Run energy_mesure')
                    print('-Run energy_mesure')
                    _thread.start_new_thread(energy_mesure.mesure.read(), ())
                else:
                    print('Disable Energy service')
                    log_service.info('Disable Energy service')

        except Exception as e:
            print('!!!! Err:{}'.format(e))
            log_service.info('!!!! Err:{}'.format(e))
            pass

        while True:
            time.sleep(60)
            print('Main loop')
        print('Exit Bootloader main')

if __name__ == '__main__':
    main()