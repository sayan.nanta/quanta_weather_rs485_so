## pi@raspberrypi:~ $ crontab -e
## @reboot sudo python3 quanta_weather_rs485_so/Bootloader.py
## @reboot sudo python3 quanta_weather_rs485_so/Bootloader_weather.py

import subprocess
import os
import time
import uuid

try:
    os.system('pwd')
    os.chdir("quanta_weather_rs485_so")
    os.system('pwd')
except:
    pass

try:
    print('Load..')
    os.system('cat Quanta_Env.py')
    import Quanta_Env
    print("\nUUID = {}\n".format(Quanta_Env.UUID))
except:
    default_uuid = '4e4e27f1-9946-4a06-89eb-999999999999'
    os.system('cp Quanta_Env.bac Quanta_Env.py')
    print('!!! Create Quanta_Env.py')
    import Quanta_Env
    ############ Generate New UUID ########################
    try:
        os.system('cat Quanta_Env.py')
        UUID = Quanta_Env.UUID
        print('\n\nUUID:{}'.format(UUID))
        # print(type(UUID))
        if UUID == default_uuid:
            print('Generate new uuid')
            New_uuid = str(uuid.uuid4())
            New_uuid = 'UUID = "'+ New_uuid + '"\n'
            a_file = open('Quanta_Env.py', "r") #type string
            list_of_lines = a_file.readlines()
            print('Old :{}'.format(list_of_lines[0]))
            list_of_lines[0] = New_uuid
            print(list_of_lines[0])
            a_file = open('Quanta_Env.py', "w")
            a_file.writelines(list_of_lines)
            a_file.close()
            print('Generate sucess:{}\nReboot system'.format(New_uuid))
            time.sleep(3)
            os.system('cat Quanta_Env.py')
            os.system('sudo reboot')

    except Exception as e:
        print('Err:{}'.format(e))
        pass

### Init SysLed
import SysLed
SysLed.led()
SysLed.led.show(1,0x008F00)
SysLed.led.show(2,0x000000)
SysLed.led.show(3,0x000000)
SysLed.led.show(4,0x000000)
# time.sleep(10)

import Constants
# import lamp_ctl
# import ipcam_ctl
import watchdog_ctl
# import weather_ctl
# import Weather_service

# import timerService
# import energy_mesure
import _thread
import MqttService as MqttService
import SendData
# import Quanta_Env


####### Set I/O on boot
# ipcam_ctl.set_state(False)
# weather_ctl.set_state(False)
# lamp_ctl.set_state(False)
# watchdog_ctl.set_state(True)
# time.sleep(15)

def check_inet():
    #### Check Internet ####
    if check_connection.checkInternetHttplib():
        Constants.INET_CON = True
        return True
    else:
        Constants.INET_CON = False
        return False

import check_connection
import LogService as LogService
log_service = LogService.LogService()

####################### Get CPU Info
try:
    
    print('get cpuinfo')
    ls_cpuinfo = subprocess.run(['tail','/proc/cpuinfo'], capture_output=True) # get last 10 line in file
    Constants.CPUINFO = ls_cpuinfo.stdout
    file = open('cpuinfo.txt', "w+b")
    # os.system('ls -la')
    file.write(ls_cpuinfo.stdout)
    file.close()
    log_service.info('-Create file cpuinfo')
    print('*** Create file cpuinfo')
    ################ write cpu model to Constansts
    ls_cpuinfo = subprocess.run(['tail','-1','/proc/cpuinfo'], capture_output=True) # get last 10 line in file
    Constants.MODEL = ls_cpuinfo.stdout
except:
    pass

name = os.uname()
MachineNodename = name.nodename
print(MachineNodename)
if 'raspberrypi' or 'Raspberrypi' in MachineNodename :
    MachineNodename = 'pi'
    print('\n\nMachine hardware is Raspberrypi')
    log_service.info('-Machine hardware is Raspberrypi')
else:
    MachineNodename = 'x86'
    print('\n\nMachine hardware is Computer')

if MachineNodename == 'pi':
    ls_file = subprocess.run(['ls'], capture_output=True)
    file = open('log_DIR.txt', "w+b")
    file.write(ls_file.stdout)
    file.close()

# if check_inet() == True :
#     try:
#         if MachineNodename == 'pi':
#             print('\nCheck new software by Davin.tech')
#             log_service.info('Check new software by Davin.tech')
#             retval = subprocess.run(['git','pull'], capture_output=True)
#             file = open('git_log.txt', "w+b")
#             print(retval.stdout)
#             file.write(file.write(retval.stdout))
#             file.close()
#     except:
#         pass

def led_blink():
    time.sleep(0.05)
    SysLed.led.show(1,0x000000)
    time.sleep(0.05)
    SysLed.led.show(1,0x001F00)

class main:
    def __init__(self):
        ### Delay For Sensor boot ready
        print(Constants.SOFTWARE_TITLE)
        led_blink()
        led_blink()
        led_blink()
        SysLed.led.show(1,0xFFFFFF)
        time.sleep(0.5)
        SysLed.led.show(2,0xFFFFFF)
        time.sleep(0.5)
        SysLed.led.show(3,0xFFFFFF)
        time.sleep(0.5)
        SysLed.led.show(4,0xFFFFFF)
        time.sleep(1)
        SysLed.led.show(1,0x00FF00)
        SysLed.led.show(2,0x0)
        SysLed.led.show(3,0x0)
        SysLed.led.show(4,0x0)
        # weather_ctl.set_state(True)
        try:
            # if Quanta_Env.STATION_INDEX == '0':
            #     if Quanta_Env.WEATHER_SERVICE == True:
            #         _thread.start_new_thread(Weather_service.SensorService, ())
            #         log_service.info('-Run Weather_service')
            #         print('-Run Weather_service')
            # else:
            #     print('Disable weather service')
            #     log_service.info('Disable weatherservice')

#             if Quanta_Env.TIMER_SERVICE == True:
#                 _thread.start_new_thread(timerService.timerService, ())
#                 log_service.info('-Run timerService')
#                 print('-Run timerService')

            while True:
                time.sleep(1)
                if check_inet():
                    if Quanta_Env.MQTT_SERVICE == True:
                        _thread.start_new_thread(MqttService.MqttService, ())
                        log_service.info('-### Run MqttService')
                        print('\n-### Run MqttService')
                    if Quanta_Env.SENDDATA_SERVICE == True:
                        _thread.start_new_thread(SendData.SendDataService, ())
                        log_service.info('-### Run SendDataService')
                        print('\n-### Run SendDataService')

                    break
                else:
                    print('!!Internet Not Connect')
                    log_service.info('!!Internet Not Connect')
                    time.sleep(5)

            # if Quanta_Env.STATION_INDEX == '0':
            #     if Quanta_Env.ENERGY_SERVICE == True:
            #         energy_mesure.mesure()
            #         log_service.info('-Run energy_mesure')
            #         print('-Run energy_mesure')
            #         _thread.start_new_thread(energy_mesure.mesure.read(), ())
            # else:
            #     print('Disable Energy service')
            #     log_service.info('Disable Energy service')

        except Exception as e:
            print('!!!! Err:{}'.format(e))
            log_service.info('!!!! Err:{}'.format(e))
            pass

        while True:
            time.sleep(60)
            print('Main loop')
        print('Exit Bootloader main')

if __name__ == '__main__':
    main()