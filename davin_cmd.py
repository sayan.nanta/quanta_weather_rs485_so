# import lamp_ctl
# import ipcam_ctl
# import send_picam , send_ipcam
import LogService as LogService
import MqttService
import Constants
import subprocess
import os
import time
import shutil
log_service = LogService.LogService()

def edit_file(MSG):
    print('cmd {}'.format(MSG))
    print('type {}'.format(type(MSG)))
    MSG = MSG.split('.')
    if 'uuid' in MSG[2] :
        fileName = 'Davin_Env.py'
        print(fileName)
        fileLine = 0 # UUID
        msg = MSG[3]
        a_file = open(fileName, "r") #type string
        list_of_lines = a_file.readlines()
        print(list_of_lines[fileLine])
        list_of_lines[fileLine] = 'UUID = \"' + msg + '\"\n'
        print(list_of_lines[fileLine])
    if 'station' in MSG[2] :
        fileName = 'Davin_Env.py'
        print(fileName)
        fileLine = 1 # Station index
        msg = MSG[3]
        a_file = open(fileName, "r") #type string
        list_of_lines = a_file.readlines()
        list_of_lines[fileLine] = 'STATION_INDEX = \"' + msg + '\"\n'
        print(list_of_lines[fileLine])
    if 'camidx' in MSG[2] :
        fileName = 'Davin_Env.py'
        print(fileName)
        fileLine = 2 # Camera index
        msg = MSG[3]
        a_file = open(fileName, "r") #type string
        list_of_lines = a_file.readlines()
        list_of_lines[fileLine] = 'CAMERA_INDEX = \"' + msg + '\"\n'
        print(list_of_lines[fileLine])

    ###### Set weather  
    if 'weather' in MSG[2] :
        fileName = 'Davin_Env.py'
        print(fileName)
        fileLine = 10 # WEATHER_SERVICE = True
        a_file = open(fileName, "r") #type string
        list_of_lines = a_file.readlines()
        print(list_of_lines[fileLine])
        if 'enable' in MSG[3]:
            list_of_lines[fileLine] = 'WEATHER_SERVICE = True\n'
        elif 'disable' in MSG[3]:
            list_of_lines[fileLine] = 'WEATHER_SERVICE = False\n'
        print(list_of_lines[fileLine]) 
    ###### Set Enargy
    if 'energy' in MSG[2] :
        fileName = 'Davin_Env.py'
        print(fileName)
        fileLine = 11 # ENERGY_SERVICE = True
        a_file = open(fileName, "r") #type string
        list_of_lines = a_file.readlines()
        print(list_of_lines[fileLine])
        if 'enable' in MSG[3]:
            list_of_lines[fileLine] = 'ENERGY_SERVICE = True\n'
        elif 'disable' in MSG[3]:
            list_of_lines[fileLine] = 'ENERGY_SERVICE = False\n'
        print(list_of_lines[fileLine]) 
    ###################### Timer ###########
    if 'timer' in MSG[2] :
        if 'lamp' in MSG[3]:
            prog = int(MSG[4])
            print(prog)
            fileName = 'timer_config.py'
            New_val = '    '+ MSG[5] + ',\n'
            fileLine = prog+1

        if 'picam' in MSG[3]:
            prog = int(MSG[4])
            print(prog)
            fileName = 'timer_config.py'
            New_val = '    '+ MSG[5] + ',\n'
            fileLine = prog+13

        if 'ipcam' in MSG[3]:
            prog = int(MSG[4])
            print(prog)
            fileName = 'timer_config.py'
            New_val = '    '+ MSG[5] + ',\n'
            fileLine = prog+25
            
        print('Write : Prog {}= {}'.format(fileLine,New_val))
        a_file = open(fileName, "r") #type string
        list_of_lines = a_file.readlines()
        list_of_lines[fileLine] = New_val
        print(list_of_lines[fileLine])

    # for i in range (0,len(list_of_lines)):
    #     print('data {}type {}'.format(list_of_lines[i],type(list_of_lines[i])))

    a_file = open(fileName, "w")
    a_file.writelines(list_of_lines)
    a_file.close()
    print('Write file success')

def os_cmd(MSG):
    print('cmd {}'.format(MSG))
    print('type {}'.format(type(MSG)))
    MSG = MSG.split('.')
    if 'chdir' in MSG[2] :
        msg = MSG[3]
        os.chdir(msg)
        print('os.chdir({})'.format(msg))
        log_service.info('os.chdir({})'.format(msg))
        os.system('pwd')
        MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'{}'.format(msg))

    if 'system' in MSG[2] :
        msg = MSG[3]
        print('os.system({})'.format(msg))
        log_service.info('os.system({})'.format(msg))
        os.system(msg)
        
    if 'listdir' in MSG[2] :
        msg = MSG[3]
        print('os.listdir({})'.format(msg))
        log_service.info('os.listdir({})'.format(msg))
        os.listdir(msg)

    if 'makedirs' in MSG[2] :
        msg = MSG[3]
        print('os.makedirs({})'.format(msg))
        log_service.info('os.makedirs({})'.format(msg))
        os.makedirs(msg)


def command(msg):
    # print(type(msg))
    if 'davin' in msg:
        print('Is davin cmd')
        log_service.info('Davin cmd: {}'.format(msg))
        if msg == 'davin.get.ip':
            msg = 'hostname -I'
        elif msg == 'davin.adjcam':
            # message = 'hostname -I'
            # message = message.split(' ')
            # sub_msg = subprocess.run(message, capture_output=True)
            # sub_msg = str(sub_msg.stdout,'utf-8')
            # sub_msg = sub_msg.replace('\n','')
            # sub_msg = sub_msg.replace(' ','')
            # print(sub_msg)
            # print(type(sub_msg))
            # MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Open by VLC , Network Stream >tcp/h264://{}:3333'.format(sub_msg))
            # log_service.info('Open by VLC , Network Stream > tcp/h264://{}:3333'.format(sub_msg))
            # print('Open by VLC , Network Stream > tcp/h264://{}:3333'.format(sub_msg))
            msg = 'raspivid -t 0 -l -rot 180 -w 800 -h 600 -b 2000000 -o tcp://0.0.0.0:3333'

        elif msg == 'davin.restore.env':
            try:
                print('Restore Davin_Env.py')
                os.system('cp Davin_Env.bac Davin_Env.py')
                MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Restore Davin_Env')
                msg = ''
            except:
                print('Restore Davin_Env.py Failt')

        elif msg == 'davin.restore.timer':
            try:
                print('Restore timer_config.py')
                os.system('cp timer_config.bac timer_config.py')
                MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Restore timer_config')
                msg = ''
            except:
                print('Restore timer_config.py Failt')
        elif msg == 'davin.get.info':
            msg = 'cat Davin_Env.py'
        elif msg == 'davin.reboot':
            msg = 'sudo reboot'
            MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Now Reboot system')
        elif msg == 'davin.get.log':
            msg = 'tail -20 /tmp/davin-info.log'
            MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'System Log 20 line')
        elif msg == 'davin.get.timer':
            msg = 'cat timer_config.py'
        elif msg == 'davin.fw.reset':
            msg = 'git reset --hard'
        elif msg == 'davin.fw.update':
            msg = 'git pull'
        elif msg == 'davin.lamp.on':
            lamp_ctl.set_state(True)
            MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Davin CMD: Turn On Lamp')
            log_service.info('Davin CMD: Turn On Lamp')
            msg = ''
        elif msg == 'davin.lamp.off':
            lamp_ctl.set_state(False)  
            MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Davin CMD: Turn Off Lamp')
            log_service.info('Davin CMD: Turn Off Lamp')
            msg = ''
        elif msg == 'davin.cap.picam':
            send_picam.sendCamService()
            MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Davin CMD: Capture Pi camera')
            log_service.info('Davin CMD: Capture Pi camera')
            msg = ''
        elif msg == 'davin.cap.ipcam':
            send_ipcam.sendCamService()
            MqttService.MqttService.publish(Constants.MQTT_TOPIC_DATA,'Davin CMD: Capture IP camera')
            log_service.info('Davin CMD: Capture IP camera')
            msg = ''
        elif 'davin.set' in msg:
            edit_file(msg)
            log_service.info('Davin CMD: Edit file')
            msg = ''
        elif 'davin.os' in msg:
            os_cmd(msg)
            log_service.info('Davin CMD: Edit file')
            msg = ''
        else:
            msg = ''
    else:
        print('forward msg')
    return msg

